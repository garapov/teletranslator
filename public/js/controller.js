$(function() {
    function clickPlus(selector, target, amount) {
        $(document).on('click', selector, function() {
            $(target).text(+$(target).text() + amount);
        })
    };
    function clickMinus(selector, target, amount) {
        $(document).on('click', selector, function() {
            $(target).text(+$(target).text() - amount);
        })
    };
    function clickStart(selector, className) {
        $(document).on('click', selector, function() {
            $(this).toggleClass(className);
        })
    };
    clickStart('button.play', 'played');
    clickPlus('.fontUp', '.fontRange .count', 1);
    clickPlus('.speedUp', '.speedRange .count', 5);
    clickMinus('.fontDown', '.fontRange .count', 1);
    clickMinus('.speedDown', '.speedRange .count', 5);
});