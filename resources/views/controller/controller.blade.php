@extends('layouts.app')

@section('name')
    Назад
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="controller_wrap">
                <button class="btn btn-default rotate">
                    <img src="{{ asset('/svg/reflection-symbol.svg') }}" alt="Отразить">
                </button>
                <button class="btn btn-default edit">
                    <img src="{{ asset('/svg/writing.svg') }}" alt="Редактировать">
                </button>
                <button class="btn btn-default restart">
                    <img src="{{ asset('/svg/refresh-button.svg') }}" alt="Перезапустить">
                </button>
                <button class="btn btn-default exit">
                    <img src="{{ asset('/svg/logout.svg') }}" alt="Перезапустить">
                </button>
                {{--<a href="{{ route('home') }}" class="btn btn-default exit">--}}
                    {{--<img src="{{ asset('/svg/logout.svg') }}" alt="Выйти">--}}
                {{--</a>--}}
                <div class="btn btn-default font_wrap">
                    <button class="fontDown"></button>
                    <div class="fontRange"><span class="count">33</span>px</div>
                    <button class="fontUp"></button>
                </div>
                <div class="btn btn-default scroll_wrap">
                    <button class="scrollUp"></button>
                    <button class="scrollDown"></button>
                </div>
                <button class="btn btn-primary play">
                    <img class="playImage" src="{{ asset('/svg/play.svg') }}" alt="Запустить">
                    <img class="pauseImage" src="{{ asset('/svg/pause.svg') }}" alt="Запустить">
                </button>
                <div class="btn btn-default speed_wrap">
                    <button class="speedDown"></button>
                    <div class="speedRange"><span class="count">250</span> wps</div>
                    <button class="speedUp"></button>
                </div>
            </div>
        </div>
    </div>
@endsection
