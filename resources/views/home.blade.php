@extends('layouts.app')

@section('name')
    Выбирите тип:
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="buttons_wrap">
            <a class="btn btn-info" href="{{ route('text') }}">Телесуфлер</a>
            <a class="btn btn-primary" href="{{ route('controller') }}">Пульт управления</a>
        </div>
    </div>
</div>
@endsection
