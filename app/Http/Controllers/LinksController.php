<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LinksController extends Controller
{
    public function text()
    {
        return view('texts.text');
    }

    public function controller()
    {
        return view('controller.controller');
    }
}
